package prgt5e20aldarias;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Fichero: Ejercicio0502.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 15-nov-2013
 */
public class Ejercicio0502 {
  /* Fibonacci recursivo
   fib(0)=0
   fib(1)=1
   fib(n)=fib(n-1)+fib(n-2)
   */

  /*
   * Fibonacci Recursivo
   */
  public static int fibonaccir(int n) {
    if (n == 0) {
      return 0;
    }
    if (n == 1) {
      return 1;
    }
    return fibonaccir(n - 1) + fibonaccir(n - 2);

  }

  /*
   * Fibonacci Iterativo
   */
  public static int fibonaccii(int num) {
    if ((num >= 1) && (num <= 2)) {
      return 1;
    }
    int este = 0, ant1, ant2;
    ant1 = ant2 = 1;
    for (int i = 2; i < num; i++) {
      este = ant1 + ant2;
      ant2 = ant1;
      ant1 = este;
    }
    return este;
  }

  public static void main(String[] args) {
    for (int i = 0; i < 10; i++) {
      System.out.println("Fibonacci Recursivo de " + i + ": " + fibonaccir(i));
    }
    System.out.println("");
    for (int i = 0; i < 10; i++) {
      System.out.println("Fibonacci Iterativo de " + i + ": " + fibonaccii(i));
    }

  }
}

/* EJECUCION:
 Fibonacci(0) Recursivo: 0
 Fibonacci(1) Recursivo: 1
 Fibonacci(2) Recursivo: 1
 Fibonacci(3) Recursivo: 2
 Fibonacci(4) Recursivo: 3
 Fibonacci(5) Recursivo: 5
 Fibonacci(6) Recursivo: 8
 Fibonacci(7) Recursivo: 13
 Fibonacci(8) Recursivo: 21
 Fibonacci(9) Recursivo: 34

 Fibonacci(0) Iterativo: 0
 Fibonacci(1) Iterativo: 1
 Fibonacci(2) Iterativo: 1
 Fibonacci(3) Iterativo: 2
 Fibonacci(4) Iterativo: 3
 Fibonacci(5) Iterativo: 5
 Fibonacci(6) Iterativo: 8
 Fibonacci(7) Iterativo: 13
 Fibonacci(8) Iterativo: 21
 Fibonacci(9) Iterativo: 34
 */
