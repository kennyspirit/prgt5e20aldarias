/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt5e20aldarias;

/**
 * Fichero: Ejercicio0501.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 15-nov-2013
 */
public class Ejercicio0501 {
  /*
   fac(1)=1
   fac(n)=fac(n-1)*n
   */

  /*
   * Factorial Recursivo 
   */
  public static int factorialr(int n) {
    if (n == 1) {
      return 1;
    } else {
      return factorialr(n - 1) * n;
    }
  }

  /*
   * Factorial Iterativo
   */
  public static int factoriali(int n) {

    int f = 1, i = 1;

    while (i <= n) {
      f = f * i;
      i++;
    }
    return f;
  }

  public static void main(String[] args) {

    System.out.println("Factorial Recursivo de 3 es: " + factorialr(3));
    System.out.println("Factorial Iterativo de 3 es: " + factoriali(3));
  }
}
/* EJECUCION:
 Factorial Recursivo de 3 es: 6
 Factorial Iterativo de 3 es: 6
 */
