/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt5e20aldarias;

/**
 * Fichero: Ejercicio0506.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */

public class Ejercicio0506 {

  private static int dimension=0;
  public static int elemento(int fila, int columna) {
    if (fila < 1 || columna < 1 ) {
      return 0;
    }
    if (columna == 1 ) {
      return 1;
    }
    return elemento(fila-1,columna)+elemento(fila-1,columna-1)+
           elemento(fila-1,columna-2);
  }
  public static void generapiramide(int numfilas) {
    dimension=numfilas;
    for (int i=1; i<(numfilas+1) ; i++) {
      for (int e=0; e<(numfilas - i); e++) {
        System.out.print(" ");
      }
      for (int j=1; j<(2*numfilas+1) ; j++) {
        int dato = elemento(i,j);
        if (dato > 0 ) {
          System.out.print(dato+" ");
        }
      }
      System.out.println("");
    }
  }
  public static int suma(int dimension) {
    int sumatorio = 0;
    for (int i=1; i<(dimension+1) ; i++) {
      for (int j=1; j<(2*dimension+1) ; j++) {
        int dato = elemento(i,j);
        if (dato > 0 ) {
          sumatorio+=dato;
        }
      }
    }
    return sumatorio;
  }
  public static void main(String[] args) {
    generapiramide(4);
    System.out.println(suma(4));
  }
}
/* EJECUCION:
   1 
  1 1 1 
 1 2 3 2 1 
1 3 6 7 6 3 1 
40
*/