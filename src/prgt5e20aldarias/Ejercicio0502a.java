
package prgt5e20aldarias;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Fichero: Ejercicio0502a.java
 * Programa que lee hasta que se introduce un numero.
 * @date 17-dic-2013
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejercicio0502a {

    public int leerNumero() {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        String linea;
        int numero = 0;

        try {
            System.out.println("Introduce");
            linea = buffer.readLine();
            numero = Integer.parseInt(linea);
            return numero;


        } catch (Exception e) {
            System.out.println("Error");
            numero=leerNumero();
            return numero;
        }

    }

    public static void main(String[] args) {
        Ejercicio0502a i = new Ejercicio0502a();
        int numero;
        numero = i.leerNumero();
        System.out.println(numero);
    }
}
