/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt5e20aldarias;

/**
 * Fichero: Ejercicio0510.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */

public class Ejercicio0510 implements Cloneable { // Pez7
  private static int numpeces=0;
  protected String nombre;
  public String getNombre() {
    return this.nombre;
  }
  public void setNombre(String s) {
    this.nombre=s;
  };
  public Object clone() {
    Object objeto=null;
    try {
      objeto =super.clone();
      numpeces++;
    } catch(CloneNotSupportedException ex) {
      System.out.println("Error al duplicar");
    }
    return objeto;
  }
  public boolean equals(Ejercicio0510 ese) {
    if (ese.getNombre() == this.getNombre()) {
      return true;
    }
    return false;
  };
  public int getpeces() {
    return this.numpeces;
  }
  Ejercicio0510() {
    numpeces++;
  }
  Ejercicio0510(Ejercicio0510 p) {
    numpeces++;
    this.nombre = p.getNombre();
  }
  public static void main(String[] args) {
    Ejercicio0510 p1 = new Ejercicio0510();
    p1.setNombre("Gulli");
    Ejercicio0510 p2 = new Ejercicio0510();
    p2.setNombre("Escalar");
    Ejercicio0510 p3 = (Ejercicio0510)p2.clone();
    Ejercicio0510 p4 = new Ejercicio0510(p1);
    System.out.println(p1.getNombre());
    System.out.println(p2.getNombre());
    System.out.println(p3.getNombre());
    System.out.println(p4.getNombre());
    System.out.println(p3.equals(p2));
    System.out.println(p3.getpeces());
  }
}
/* EJECUCION:
Gulli
Escalar
Escalar
Gulli
true
4
*/