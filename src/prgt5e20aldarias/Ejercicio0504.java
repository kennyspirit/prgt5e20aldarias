/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt5e20aldarias;

// Importamos el paquete donde se encuentra la clase Point
import java.awt.*;

/**
 * Fichero: Punto3d.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 15-nov-2013
 */
public class Ejercicio0504 extends Point {

  /* Con extends Points convertimos nuestra clase en subclase de
   * la clase Points
   * */

  /* Creamos la variable para la tercera coordenada, ya que x e y las
   * recibe de su superclase.
   * */
  public int z;

  /* Creamos el metodo "Constructor de la clase" (debe tener el mismo
   * nombre q la clase en que se encuentra). Le pasamos como parametros
   * los enteros que usamos para las coordenadas.
   * */
  public Ejercicio0504(int x, int y, int z) {
    /* Declarcion que usa una palabra clave. Hace referencia a la
     * superclase del objeto actual
     * */
    super(x, y);
    /* Con this nos referimos al objeto generico que en ese momento
     * esta usando la clase
     * */
    this.z = z;
  }

  public void move(int x, int y, int z) {
    this.z = z;
    super.move(x, y);
  }

  //Reescribimos el metodo translate para 3 coordenadas
  public void translate(int x, int y, int z) {
    this.z += z;
    this.x += x;
    this.y += y;
    //super.translate(x, y);
  }
}

/* Esta clase no tiene main, podremos usarla en cualquier programa
 * que necesite puntos de referencia en 3D.
 * Esta subclase de la clase Point, dibuja un punto en el espacio 3D.
 * */
