/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt5e13;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-nov-2013
 */
public class Main {

  public static void main(String argv[]) {

    BaseDatos bd1;
    bd1 = new Mysql();
    bd1.open();
    bd1.close();

    BaseDatos bd2;
    bd2 = new Oracle();
    bd2.open();
    bd2.close();

  }
}
