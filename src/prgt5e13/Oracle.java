/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt5e13;

/**
 * Fichero: Mysql.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-nov-2013
 */
public class Oracle implements BaseDatos {

  @Override
  public void open() {
    Constantes c = new Constantes();
    System.out.println("Abierta conexión a oracle.");
    System.out.println("Usuario: " + c.USUARIOORACLE);
    System.out.println("Contraseña: " + c.CONSTRASENYAORACLE);
  }

  @Override
  public void close() {
    System.out.println("Cerrada conexión a oracle");
  }
}
