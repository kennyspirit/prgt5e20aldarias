/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt5e14;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 02-dic-2013
 */
public class Main {

  public static void main(String argv[]) {
    Vehiculo v = new Vehiculo();
    Coche o = new Coche();
    Camion a = new Camion();

    System.out.println("Vehiculo Generico. Contador: " + v.getId() + " Matricula: " + v.getMatricula());
    System.out.println("Vehiculo Coche.    Contador: " + o.getId() + " Matricula: " + o.getMatricula() + " Cabrio: " + o.getCabrio());
    System.out.println("Vehiculo Camion.   Contador: " + a.getId() + " Matricula: " + a.getMatricula() + " Tara: " + a.getTara());

  }
}
