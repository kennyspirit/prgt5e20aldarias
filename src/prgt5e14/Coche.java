/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt5e14;

/**
 * Fichero: Coche.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 02-dic-2013
 */
public class Coche extends Vehiculo {

  private String cabrio;

  Coche() {
    // id = 1; //error es private
    matricula = "B";
    cabrio = "si";
  }

  /**
   * @return the cabrio
   */
  public String getCabrio() {
    return cabrio;
  }

  /**
   * @param cabrio the cabrio to set
   */
  public void setCabrio(String cabrio) {
    this.cabrio = cabrio;
  }
}
