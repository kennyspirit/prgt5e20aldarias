/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt5e15.controlador;

import java.io.IOException;
import prgt5e15.modelo.Persona;
import prgt5e15.vista.Vista;
import prgt5e15.vista.VistaPersona;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 05-dic-2013
 */
public class Main {

  public static void main(String argv[]) throws IOException {

    Persona persona = new Persona();
    VistaPersona vistapersona = new VistaPersona();
    Vista vista = new Vista();

    int op;

    do {
      op = vista.menu();
      switch (op) {
        case 1:
          persona = vistapersona.getPersona();
          break;
        case 2:
          vistapersona.muestraPersona(persona);
          break;
      }
    } while (op != 0);
  }
}

/* Ejecucion:
   
 */
