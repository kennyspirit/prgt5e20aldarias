/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt5e15.vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Fichero: Vista.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 06-feb-2014
 */
public class Vista {

  public int menu() throws IOException {

    int op;
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    String linea;

    System.out.println("Menu");
    System.out.println("0. Salir");
    System.out.println("1. Leer");
    System.out.println("2. Escribir");
    System.out.print("Opcion?;");
    linea = buffer.readLine();
    op = Integer.valueOf(linea);
    return op;
  }
}
