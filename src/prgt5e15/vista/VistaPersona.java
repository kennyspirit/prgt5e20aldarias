/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt5e15.vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import prgt5e15.modelo.Persona;

/**
 * Fichero: VistaPersona.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 05-dic-2013
 */
public class VistaPersona {

  public Persona getPersona() throws IOException {
    Persona persona = new Persona();

    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    String linea;

    System.out.print("Nombre?: ");
    linea = buffer.readLine();

    persona.setNombre(linea);
    return persona;
  }

  public void muestraPersona(Persona persona) {
    System.out.println("Persona: " + persona.getNombre());
  }
}
