/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt5e15.modelo;

/**
 * Fichero: Persona.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 05-dic-2013
 */
public class Persona {

  private String nombre;

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
}
